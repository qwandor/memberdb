<?php
require 'config.php';
require 'common.php';

$action = $_POST['action'];

$dbconn = pg_connect("host=$database_host dbname=$database_name user=$database_user password=$database_password") or die('Could not connect:' . pg_last_error());

$result = '';

if ($action == 'update') {
	//Add the person to the database
	$values = map_from_post(array('firstname' => 'string', 'lastname' => 'string', 'student_id' => 'int', 'email' => 'string', 'blog' => 'string', 'homephone' => 'string', 'cellphone' => 'string', 'address' => 'string', 'notes' => 'string', 'mailing_list' => 'boolean', 'public_details' => 'boolean'));
	$values['provisional'] = true;
	if (!add_person($values)) {
		$result = "	<p class=\"error\">Error adding person: " . pg_last_error() . "</p>\n";
	}
	else {
		//Updated successfully
		$result = "	<p class=\"success\">Thanks for signing up, {$values['firstname']} {$values['lastname']}.</p>\n";
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="en-NZ">
<head>
	<title><?php echo $group_name; ?> member database</title>
	
	<link type="text/css" rel="stylesheet" href="../common.css"/>
	<link type="text/css" rel="stylesheet" href="../blue.css" title="Blue"/>
</head>

<body>
	<h1>Join <?php echo $group_name; ?></h1>
<?php
echo $result;

$date_parts = getdate();
$defaults = array('firstname' => '', 'lastname' => '', 'student_id' => '', 'email' => '', 'blog' => '', 'homephone' => '', 'cellphone' => '', 'address' => '', 'notes' => '', 'mailing_list' => true, 'public_details' => true, 'committee' => false);
person_form('add.php', $defaults, false);
?>
	<p>
		<a href="index.php">Return to list without saving</a>
	</p>
</body>
</html>
