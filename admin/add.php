<?php
require '../config.php';
require '../common.php';

$action = $_POST['action'];

$dbconn = pg_connect("host=$database_host dbname=$database_name user=$database_user password=$database_password") or die('Could not connect:' . pg_last_error());

$result = '';

if ($action == 'update') {
	//Add the person to the database
	$values = map_from_post(array('firstname' => 'string', 'lastname' => 'string', 'student_id' => 'int', 'email' => 'string', 'blog' => 'string', 'homephone' => 'string', 'cellphone' => 'string', 'address' => 'string', 'notes' => 'string', 'mailing_list' => 'boolean', 'public_details' => 'boolean', 'committee' => 'boolean', 'provisional' => 'boolean', 'first_year' => 'int', 'last_year' => 'int', 'wiki_name' => 'string'));
	if (!add_person($values)) {
		$result = "	<p class=\"error\">Error adding person: " . pg_last_error() . "</p>\n";
	}
	else {
		//Updated successfully
		$result = "	<p class=\"success\">Successfully added person {$values['firstname']} {$values['lastname']}.</p>\n";
		
		//Subscribe them to the mailing list if appropriate
		if ($values['mailing_list'] && !empty($values['email'])) {
			$name_email = "{$values['firstname']} {$values['lastname']} <{$values['email']}>";
			if (mailinglist_subscribe(array($name_email))) {
				$result .= "	<p class=\"success\">Subscribed " . htmlspecialchars($name_email) . " to mailing list too.</p>\n";
			}
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="en-NZ">
<head>
	<title><?php echo $group_name; ?> member database administration</title>
	
	<link type="text/css" rel="stylesheet" href="../common.css"/>
	<link type="text/css" rel="stylesheet" href="../blue.css" title="Blue"/>
</head>

<body>
	<h1>Member database administration</h1>
<?php
echo $result;

$date_parts = getdate();
$defaults = array('firstname' => '', 'lastname' => '', 'student_id' => '', 'email' => '', 'blog' => '', 'homephone' => '', 'cellphone' => '', 'address' => '', 'notes' => '', 'mailing_list' => false, 'public_details' => false, 'committee' => false, 'provisional' => false, 'first_year' => $date_parts['year'], 'last_year' => $date_parts['year'], 'wiki_name' => '');
person_form('add.php', $defaults);
?>
	<p>
		<a href="index.php">Return to list without saving</a>
	</p>
</body>
</html>
