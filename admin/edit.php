<?php
require '../config.php';
require '../common.php';

$person = (int) $_GET['person'];
$action = $_POST['action'];

$dbconn = pg_connect("host=$database_host dbname=$database_name user=$database_user password=$database_password") or die('Could not connect:' . pg_last_error());

$result = '';

if ($action == 'update') {
	$values = map_from_post(array('firstname' => 'string', 'lastname' => 'string', 'student_id' => 'int', 'email' => 'string', 'blog' => 'string', 'homephone' => 'string', 'cellphone' => 'string', 'address' => 'string', 'notes' => 'string', 'mailing_list' => 'boolean', 'public_details' => 'boolean', 'committee' => 'boolean', 'provisional' => 'boolean', 'first_year' => 'int', 'last_year' => 'int', 'wiki_name' => 'string'));
	if (!update_person($person, $values)) {
		$result = "	<p class=\"error\">Error updating person $person: " . pg_last_error() . "</p>\n";
	}
	else {
		//Subscribe them to the mailing list if appropriate
		if ($values['mailing_list'] && !empty($values['email'])) {
			$name_email = "{$values['firstname']} {$values['lastname']} <{$values['email']}>";
			mailinglist_subscribe(array($name_email));
		}
		
		//Updated successfully, redirect back to list
		header("Location: index.php");
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="en-NZ">
<head>
	<title><?php echo $group_name; ?> member database administration</title>
	
	<link type="text/css" rel="stylesheet" href="../common.css"/>
	<link type="text/css" rel="stylesheet" href="../blue.css" title="Blue"/>
</head>

<body>
	<h1>Member database administration</h1>
	
<?php
echo $result;

$query_result = pg_query("SELECT * FROM people WHERE id = $person");

if (pg_num_rows($query_result) < 1) {
	echo "<p class=\"error\">No such person $person.</p>";
}
else {
	$row = pg_fetch_assoc($query_result);
	person_form("edit.php?person=$person", $row);
}
?>
	<p>
		<form method="post" action="index.php">
			<input type="hidden" name="action" value="delete" />
			<input type="hidden" name="person" value="<?php echo $person; ?>" />
			<input type="submit" value="Delete person" />
		</form>
	</p>
	<p>
		<a href="../vcard.php?person=<?php echo $person; ?>&amp;token=<?php echo $full_view_token; ?>">Download vCard</a>
	</p>
	<p>
		<a href="index.php">Return to list without saving</a>
	</p>
</body>
</html>
