<?php
require '../config.php';
require '../common.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="en-NZ">
<head>
	<title><?php echo $group_name; ?> member database administration</title>
	
	<link type="text/css" rel="stylesheet" href="../common.css"/>
	<link type="text/css" rel="stylesheet" href="../blue.css" title="Blue"/>
</head>

<body>
	<h1>Member database administration</h1>
	
	<p>Click on somebody's name to edit their details.</p>
	
<?php
function yesno($value) {
	return $value == 't' ? '<img src="../membericons/yes.png" alt="yes" />' : '<img src="../membericons/no.png" alt="no" />';
}

$action = $_POST['action'];

$dbconn = pg_connect("host=$database_host dbname=$database_name user=$database_user password=$database_password") or die('Could not connect:' . pg_last_error());

if ($action == 'delete') {
	$person = (int) $_POST['person'];
	if (!pg_query("DELETE FROM people WHERE id = $person")) {
		echo "<p class=\"error\">Error deleting person $person: " . pg_last_error() . "</p>";
	}
	else {
		echo "<p class=\"success\">Person $person deleted.</p>";
	}
}
else if ($action == 'sync_mailman') {
	$mailing_list_query_result = pg_query("SELECT COALESCE(firstname, '') || ' ' || COALESCE(lastname, '') || ' <' || email || '>' AS name_email FROM people WHERE last_year >= 2008 AND email IS NOT NULL AND mailing_list");
	$subscribees = pg_fetch_all_columns($mailing_list_query_result, 0);
	if (mailinglist_subscribe($subscribees)) {
		echo "<p class=\"success\">Subscribed " . count($subscribees) . " people to mailing list.</p>";
	}
	else {
		echo "<p class=\"warning\">Did not subscribe " . count($subscribees) . " people to mailing list.</p>";
	}
}

$year = isset($_GET['year']) ? (int) $_GET['year'] : null;

$query_result = pg_query("SELECT * FROM people" . (is_null($year) ? '' : " WHERE first_year <= $year AND $year <= last_year") . " ORDER BY firstname, lastname");

?>
	<table class="bordered">
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Student ID</th>
			<th scope="col">Email</th>
			<th scope="col">Home phone</th>
			<th scope="col">Cell phone</th>
			<th scope="col">Address</th>
			<th scope="col">Mailing list</th>
			<th scope="col">Visible</th>
			<th scope="col">Committee</th>
			<th scope="col">Provisional</th>
			<th scope="col">Notes</th>
			<th scope="col">First year</th>
			<th scope="col">Last year</th>
		</tr>
<?php
while ($row = pg_fetch_assoc($query_result)) {
	echo "		<tr class=\"vcard\">\n";
	echo "			<td class=\"fn\"><a href=\"edit.php?person={$row['id']}\">{$row['firstname']} {$row['lastname']}</a></td>\n";
	echo "			<td>{$row['student_id']}</td>\n";
	echo "			<td class=\"email\"><a href=\"mailto:{$row['firstname']} {$row['lastname']} &lt;{$row['email']}&gt;\">{$row['email']}</a></td>\n";
	echo "			<td class=\"tel\"><span class=\"type\">home</span><span class=\"value\">{$row['homephone']}</span></td>\n";
	echo "			<td class=\"tel\"><span class=\"type\">cell</span><span class=\"value\">{$row['cellphone']}</span></td>\n";
	echo "			<td>{$row['address']}</td>\n";
	echo "			<td>" . yesno($row['mailing_list']) . "</td>\n";
	echo "			<td>" . yesno($row['public_details']) . "</td>\n";
	echo "			<td>" . yesno($row['committee']) . "</td>\n";
	echo "			<td>" . yesno($row['provisional']) . "</td>\n";
	echo "			<td>{$row['notes']}</td>\n";
	echo "			<td>{$row['first_year']}</td>\n";
	echo "			<td>{$row['last_year']}</td>\n";
	echo "		</tr>\n";
}
?>
	</table>
	
	<p><a href="add.php">Add person</a></p>
<?php
$date_parts = getdate();
$current_year = $date_parts['year'];
if ($current_year == $year) {
?>
	<p><a href="?">Show members for all years</a></p>
<?php
}
else {
?>
	<p><a href="?year=<?php echo $current_year; ?>">Limit to <?php echo $current_year; ?> members</a></p>
<?php
}
?>
	<p><a href="../vcard.php?token=<?php echo $full_view_token; ?>">Download all</a></p>
<?php
if (!empty($mailman_base_url)) {
?>
	<p>
		<form method="post" action="index.php">
			<input type="hidden" name="action" value="sync_mailman" />
			<input type="submit" value="Synchronise all with mailing list" />
		</form>
	</p>
<?php
}
?>
</body>
</html>
