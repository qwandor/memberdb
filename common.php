<?php
require 'config.php';

//Return PostgreSQL string literal or null value, escaping as necessary
function pg_string($string, $empty_to_null = false) {
	if (is_null($string) || ($empty_to_null && $string == '')) return 'NULL';
	else return '\'' . pg_escape_string($string) . '\'';
}

function pg_value($value, $empty_string_to_null = false) {
	if (is_bool($value)) return $value ? 'true' : 'false';
	else if (is_int($value)) return "$value";
	else return pg_string($value, $empty_string_to_null);
}

function update_person($person, $columns) {
	$updates = array();
	foreach ($columns as $name => $value) {
		$updates[] = $name . '=' . pg_value($value, true);
	}
	return pg_query("UPDATE people SET " . implode(', ', $updates) . ", updated=now() WHERE id = $person");
}

function add_person($columns) {
	$names = array();
	$values = array();
	foreach ($columns as $name => $value) {
		$names[] = $name;
		$values[] = pg_value($value, true);
	}
	//added and updated columns automatically set to defaults (current time)
	return pg_query("INSERT INTO people (" . implode(', ', $names) . ") VALUES(" . implode(', ', $values) . ")");
}

//Construct and return an array with the given names as keys and the values taken from the $_POST array with the given types
function map_from_post($names) {
	$array = array();
	foreach ($names as $name => $type) {
		if ($type == 'boolean') {
			$array[$name] = ($_POST[$name] == 'true') ? true : false;
		}
		else if ($type == 'int') {
			$array[$name] = strlen($_POST[$name]) == 0 ? null : (int) $_POST[$name];
		}
		else { //Assume string
			$array[$name] = $_POST[$name];
		}
	}
	return $array;
}

//Form to add or edit people
//If $admin is false show a limited version without all fields
function person_form($action, $row, $admin = true) {
	global $group_name;
?>
	<form method="post" action="<?php echo $action; ?>">
		<div><input type="hidden" name="action" value="update" /></div>
		<table class="bordered">
			<tr>
				<th scope="row">First name</th>
				<td><input type="text" name="firstname" value="<?php echo $row['firstname']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Last name</th>
				<td><input type="text" name="lastname" value="<?php echo $row['lastname']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Student ID</th>
				<td><input type="text" name="student_id" value="<?php echo $row['student_id']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Email address</th>
				<td><input type="text" name="email" size="50" value="<?php echo $row['email']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Blog URL</th>
				<td><input type="text" name="blog" size="50" value="<?php echo $row['blog']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Home phone number</th>
				<td><input type="text" name="homephone" value="<?php echo $row['homephone']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Cellphone number</th>
				<td><input type="text" name="cellphone" value="<?php echo $row['cellphone']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Address</th>
				<td><input type="text" name="address" size="50" value="<?php echo $row['address']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Notes</th>
				<td><textarea name="notes" rows="4" cols="50"><?php echo $row['notes']; ?></textarea></td>
			</tr>
			<tr>
				<th scope="row">Mailing list</th>
				<td>
					<input type="checkbox" name="mailing_list" value="true"<?php echo $row['mailing_list'] == 't' ? ' checked="checked"' : ''; ?> />
					Add this email address to the <?php echo $group_name; ?> mailing list, to receive announcements of our activities.
				</td>
			</tr>
			<tr>
				<th scope="row">Visible to members</th>
				<td>
					<input type="checkbox" name="public_details" value="true"<?php echo $row['public_details'] == 't' ? ' checked="checked"' : ''; ?> />
					Make basic contact details available to everyone in <?php echo $group_name; ?>.
				</td>
			</tr>
<?php
if ($admin) {
?>
			<tr>
				<th scope="row">On committee</th>
				<td><input type="checkbox" name="committee" value="true"<?php echo $row['committee'] == 't' ? ' checked="checked"' : ''; ?> /></td>
			</tr>
			<tr>
				<th scope="row">Provisional member (added self)</th>
				<td><input type="checkbox" name="provisional" value="true"<?php echo $row['provisional'] == 't' ? ' checked="checked"' : ''; ?> /></td>
			</tr>
			<tr>
				<th scope="row">First year in <?php echo $group_name; ?></th>
				<td><input type="text" name="first_year" value="<?php echo $row['first_year']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Last year in <?php echo $group_name; ?></th>
				<td><input type="text" name="last_year" value="<?php echo $row['last_year']; ?>" /></td>
			</tr>
			<tr>
				<th scope="row">Wiki username</th>
				<td><input type="text" name="wiki_name" value="<?php echo $row['wiki_name']; ?>" /></td>
			</tr>
<?php
}
?>
		</table>
		<p>
			<input type="submit" value="Save" />
			<input type="reset" value="Reset" />
		</p>
	</form>
<?php
}

//If configured to integrate with Mailman, subscribe the given list of people to the mailing list
//Return true on success
function mailinglist_subscribe($subscribees) {
	global $mailman_base_url, $mailman_admin_password;
	if (!empty($mailman_base_url)) {
		//Actually subscribe people
		$result = http_post_fields("$mailman_base_url/members/add", array('adminpw' => $mailman_admin_password, 'subscribe_or_invite' => '0', 'send_welcome_msg_to_this_batch' => '1', 'send_notifications_to_list_owner' => '1', 'subscribees' => implode("\n", $subscribees)), array(), array());
		
		//TODO: do something useful with the result, maybe displaying errors and successes
		
		/*$ch = curl_init("$mailman_base_url/members/add");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('subscribe_or_invite' => '0', 'send_welcome_msg_to_this_batch' => '1', 'send_notifications_to_list_owner' => '1', 'subscribees' => $subscribees));
		
		curl_exec($ch);
		curl_close($ch);*/
		
		return true;
	}
	else {
		return false;
	}
}
?>
