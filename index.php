<?php
require 'config.php';

$dbconn = pg_connect("host=$database_host dbname=$database_name user=$database_user password=$database_password") or die('Could not connect:' . pg_last_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="en-NZ">
<head>
	<title><?php echo $group_name; ?> member list</title>
	
	<link type="text/css" rel="stylesheet" href="common.css"/>
	<link type="text/css" rel="stylesheet" href="blue.css" title="Blue"/>
</head>

<body>
	<h1>Member list</h1>
	
	<p>Click on somebody's name to download their vCard.</p>
	
<?php
$query_result = pg_query("SELECT * FROM people WHERE public_details ORDER BY firstname, lastname");

?>
	<table class="bordered">
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Email</th>
			<th scope="col">Home phone</th>
			<th scope="col">Cell phone</th>
		</tr>
<?php
while ($row = pg_fetch_assoc($query_result)) {
	echo "		<tr class=\"vcard\">\n";
	echo "			<td class=\"fn\"><a href=\"vcard.php?person={$row['id']}\">{$row['firstname']} {$row['lastname']}</a></td>\n";
	echo "			<td class=\"email\"><a href=\"mailto:{$row['firstname']} {$row['lastname']} &lt;{$row['email']}&gt;\">{$row['email']}</a></td>\n";
	echo "			<td class=\"tel\"><span class=\"type\">home</span><span class=\"value\">{$row['homephone']}</span></td>\n";
	echo "			<td class=\"tel\"><span class=\"type\">cell</span><span class=\"value\">{$row['cellphone']}</span></td>\n";
	echo "		</tr>\n";
}
?>
	</table>
	
	<p><a href="vcard.php">Download all</a></p>
	<p><a href="add.php">Join</a></p>
</body>
</html>
