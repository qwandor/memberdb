<?php
require 'config.php';

$dbconn = pg_connect("host=$database_host dbname=$database_name user=$database_user password=$database_password") or die('Could not connect:' . pg_last_error());

$person_id = is_null($_GET['person']) ? null : (int) $_GET['person'];
$token = $_GET['token'];

$query_result = pg_query("SELECT * FROM people WHERE " . (is_null($person_id) ? 'true' : "id = $person_id") . ($token == $full_view_token ? '' : " AND public_details"));

if (pg_num_rows($query_result) < 1) {
	exit("Requested person not found.");
}

header('Content-type: text/x-vcard; charset=utf-8');

while ($row = pg_fetch_assoc($query_result)) {
	echo "BEGIN:VCARD\n";
	echo "VERSION:3.0\n";
	echo "CLASS:PUBLIC\n";
	echo "CATEGORIES:$group_name\n";
	if ($row['committee'] == 't') echo "CATEGORIES:$group_name committee\n";
	echo "FN:{$row['firstname']} {$row['lastname']}\n";
	echo "N:{$row['lastname']};{$row['firstname']};;;\n";
	if (!is_null($row['homephone'])) echo "TEL;TYPE=HOME:{$row['homephone']}\n"; //Should be in X.500 format
	if (!is_null($row['email'])) echo "EMAIL:{$row['email']}\n";
	if (!is_null($row['cellphone'])) echo "TEL;TYPE=CELL:{$row['cellphone']}\n"; //Should be in X.500 format
	if (!is_null($row['blog'])) echo "URL:{$row['blog']}\n";
	else if (!is_null($row['wiki_name'])) echo "URL:{$wiki_base}{$row['wiki_name']}\n";
	echo "END:VCARD\n\n";
}
?>
